
"""
Simulate (naïvely!) mutational processes on a (by default) diploid genome
with a specifiable number of chromosomes and chromosome sizes followed by
next-gen sequencing read generation and alignment.

This script's goal is the generation of simulated data for sanity-checking
_basic correctness_ of computational tools' handling of sequencing data and,
in particular, coordinate to sequence relations. It should, for example, be
easy to catch off-by-one errors with this script.

Constraints can be added to insure the existence in otherwise random
sequence data of classes of variation for the purpose of testing other code.

It is _not_ intended to test:
1. statistical methods
2. mutational models
3. scalability. (It generates small-ish simulated data.)

The overriding goal is simplicity of use for sanity checking with genuinely
random data.

ASSUMES:
1. bwa is in your PATH
2. samtools is in your PATH

Data NOT yet simulated:
1. Errors in the sequencing; reads are currently perfect.
2. Quality measures. All generated FASTA have quality 'I'
"""

from sys import argv,stdin,stdout,stderr,exit
import argparse
from os import environ #,getcwd
from re import split as re_split, match as re_match
from random import seed,uniform,choice,random,randrange,gauss
from subprocess import run as run_child
from warnings import warn
from os.path import join as path_join, isdir, isfile
from os import mkdir
from shutil import which
from collections import namedtuple
from math import ceil,log

FragSpec = namedtuple("FragSpec",["contig","homolog","offset","length"])

_BWA = which("bwa")
if _BWA is None:
	raise RuntimeError("bwa is not in your PATH")


def random_base_qual( n, args ):
	if args.discrete_quals:
		q = choice(args.discrete_quals)
		return ''.join(                       q for _ in range(n) )
	else:
		return ''.join( chr(0x30+randrange(50)) for _ in range(n) )

#_HAP_ENC = ("mat","pat")

# Incorporate environment variables.

if "BASE_P" in environ:
	_BASES_P = [ float(v) for v in re_split( "[ ,/]", environ("BASE_P"), 3 ) ]
else:
	_BASES_P = [0.25,0.25,0.25,0.25]

_BASES_CUM_P = [ sum(_BASES_P[:i]) for i in range(len(_BASES_P)) ]
_BASE_SUM_P = sum(_BASES_P)


def _is_indexed( genome_path ) -> bool:
	return isfile( genome_path + ".bwt" )


def _emit_summary( args, fp ):
	"""
	Simply summarize all arguments in prose.
	"""
	print( "# \"{}\" has {} chromosomes with total nucleotide length {}".format(
		args.ref,
		args.contig_count,
		args.genome_length ),
		file=fp )
	print( "# Fragment lengths are distributed as N({},{})".format( *args.tlen ),
		file=fp )
	print( "# {}-end read lengths are distributed as N({},{})".format( 
		"Paired" if not args.single_end else "Single", *args.rdlen ),
		file=fp )
	print( "# \"{}\" is sequenced {}".format( 
		args.subject,
		"with exactly {} reads".format(args.rdnum) 
			if args.depth is None 
			else "to an expected mean depth of {}".format(args.depth) ),
		file=fp )
	print( "# Seed:", "unspec" if args.seed is None else args.seed, file=fp )
	# TODO: Also summarize in machine-readable form for downstream scripts.


class SplitCommaSepInts(argparse.Action):
	EXC_MSG = "requires exactly 2 comma-separated integers"
	def __init__( self, option_strings, dest, nargs=None, **kwargs ):
		"""
		const=None, default=None, type=None, choices=None, required=False, help=None, metavar=None
		"""
		#print( "SplitCommaSepInts.__init__(", option_strings, dest, nargs, ','.join( "{}={}".format( k, v ) for k,v in kwargs.items() ), ")" )
		if nargs is not None:
			raise ValueError("nargs not allowed")
		super(SplitCommaSepInts, self).__init__(option_strings, dest, **kwargs)

	def __call__(self, parser, namespace, values, option_string=None):
		#print('SplitCommaSepInts.__call__( %r %r %r)' % (namespace, values, option_string))
		try:
			value_list = [ int(v) for v in values.split(',') ]
		except ValueError as e:
			# 'from None' _replaces_ the original cause with our more specific msg
			raise ValueError("{} {}".format( option_string, SplitCommaSepInts.EXC_MSG ) ) from None
		if option_string in ['-T','--tlen','-R','--rdlen'] and len(value_list) > 2:
			raise ValueError("{} {}".format( option_string, SplitCommaSepInts.EXC_MSG ) )
		setattr( namespace, self.dest, value_list )


############################################################################

class Complementer:
	def __getitem__( self, i ):
		assert isinstance(i,int)
		upcase = i&(~(0b100000))
		if   upcase == 65: # ord('A'):
			ch = 'T'
		elif upcase == 67: # ord('C'):
			ch = 'G'
		elif upcase == 71: # ord('G'):
			ch = 'C'
		else:
			ch = 'A'
		return ch

_complementer = Complementer()

def _revcomp( seq ):
	return ''.join(reversed(seq.translate(_complementer)))

def _clamp( value, max_value ):
	return min( value, max_value )

def _random_base():
	"""
	Draw a random base given the (not necessarily uniform) probability masses
	in the _BASES_P.
	"""
	global _BASE_SUM_P,_BASES_CUM_P
	rn = uniform(0,_BASE_SUM_P)
	for i in range(len(_BASES_CUM_P)-1,0,-1):
		if rn > _BASES_CUM_P[i]:
			return "ACGT"[i]
	return "A"

def _print_fasta( seq, name="random", width=70, dest=stdout ):
	print( ">{}".format( name ), file=dest )
	i = 0
	while i < len(seq):
		line_len = min(width,len(seq)-i)
		print( "".join( seq[i:(i+line_len)] ), file=dest )
		i += line_len


def _load_fasta( filename ):
	sequences = []
	seq = []
	identifier = None
	with open( filename ) as fp:
		while True:
			line = fp.readline()
			if len(line) < 1:
				if len(seq) > 0:
					sequences.append( (identifier,seq) )
				break
			if line.startswith('>'):
				if identifier: # is not None
					sequences.append( (identifier,seq) )
					seq = []
				identifier = re_match(r'^> *(?P<ident>[^\s]+)',line).group(1)
				continue
			seq.extend( list(line.rstrip()) )
	return sequences


def _generate_variation( args, genome ):
	"""
	Generate and apply mutations according to supplied statistical parameters.
	num SNVs (het-ref, het-alt, hom-alt)
	num indels
	"""
	variants = list()
	# TODO: These are currently het-ref by default (unless a SNV _happens_
	# to hit both sequences of a given position).
	for snv in range(round(args.snvs if args.snvs >= 1.0 else args.snvs*args.genome_length)):
		contig = randrange( args.contig_count )
		# Hit _either_ of homologous chromosomes unless mutations are to be
		# confined to one particular homolog
		homolog = int( random() < args.var_bias )
		offset = randrange( args.contig_lengths[contig] )
		ref_base = genome[contig][homolog][offset]
		alternates = set("ACGT")
		alternates.discard(ref_base)
		alt_base = choice( list(alternates) )
		genome[contig][homolog][offset] = alt_base
		variants.append( ( contig, homolog, offset, ref_base, alt_base ) )

	variants.sort()
	with open( path_join( args.wdir, 'variants.tab' ), "w" ) as fp:
		for v in variants:
			print( v[0], v[1], v[2]+1, v[3], v[4], sep="\t", file=fp )
	return variants


def _contains_variant( variants, frag:FragSpec ) -> bool:
	for v in variants:
		if v[0] == frag.contig and v[1] == frag.homolog and frag.offset <= v[2] and v[2] < frag.length:
			return True
	return False


def _random_fragment( args, subject ) -> (str,FragSpec):
	"""
	Generate a random fragment respecting
	1. distribution constraints on fragment length and
	2. other constraints on fragment location.
	Return the fragment (forward) sequence verbatim and the 0-based, half-open
	specification.
	"""
	# Use reference chromosome length, though indels may have changed it!
	# Choose random contig, homolog, position and fragment length.
	contig = randrange( args.contig_count ) if args.mzone is None else args.mzone[0]
	homolog = int( random() < args.frag_bias )
	tlen = round( gauss( *args.tlen ) )
	contig_length = len(subject[contig][homolog])
	start_offset = randrange( max( 1, contig_length - tlen ) )

	# Generate the fragment from which read(s) will be taken.
	flen = _clamp( tlen, contig_length - start_offset )
	attempts = 0
	while attempts < 3:
		attempts += 1
		fragment = "".join( subject[contig][homolog][start_offset:(start_offset+flen)] )
		if fragment[0] != 'N' and fragment[-1] != 'N':
			return (fragment, FragSpec( contig, homolog, start_offset, flen ))
	return (None,None)


def _simulate_sequencing( args, subject, fp:"(fragments,fastq1[,fastq2])", variants=None ):
	"""
	Simulate either single- or paired-end reads according to whether the file
	argument is a single file handle or a tuple pair of file handles.
	Generates one or two FASTQ files.
	Notes:
	coverage = n_reads * rd_len / genome_size
	n_reads = coverage * genome_size / rd_len
	"""

	paired_end = len(fp) > 2

	# How many templates are actually required? Either an absolute count has
	# been supplied or we calculate a count from a target depth.

	N = args.rdnum if args.depth is None else \
			round(
				args.depth*args.genome_length
				/ min(args.tlen[0],args.genome_length) )

	# Make the read ID format the minimum number of hexadecimal digits
	# required to represent N.
	read_id_template = "RDX{{:0{}X}}".format( ceil(log(N+1,16)) )

	random_rdlen = lambda L:_clamp( L, round( gauss( *args.rdlen ) ) )
	# TODO: Control in paired-end case whether or not they overlap?

	# Preceding insures that target depth is achieved exactly in the (unusual)
	# case that read lengths are greater than a chromosome length.

	# Generate fragments, then reads from the fragments.

	format_bounds = lambda b:"[{},{}]({}bp)".format( *b, b[1]-b[0]+1 )

	for i in range(N):

		fragment,spec = _random_fragment( args, subject )
		if fragment is None:
			continue

		fragment_has_variant = (variants is not None) and _contains_variant( variants, spec )

		print( spec.contig, spec.homolog,
			format_bounds( ( spec.offset+1, spec.offset + spec.length ) ),
			sep="\t", end="\t", file=fp[0] ) # ...finished below!

		# Now generate read(s) from the fragment.

		rdlen = random_rdlen( len(fragment) )

		if paired_end: # paired-end reads
			# 1st
			closed_bounds = ( spec.offset                  +1, spec.offset + rdlen )
			print( format_bounds(closed_bounds), sep="\t", end="\t", file=fp[0] )
			base_quals = random_base_qual( rdlen, args )
			if args.snp_base_quals and fragment_has_variant:
				pass # TODO: 
			print( "@{}\n{}\n+\n{}".format( read_id_template.format(i),
				fragment[:rdlen],
				base_quals), file=fp[1] )

			# 2nd
			rdlen = random_rdlen( len(fragment) )
			closed_bounds = ( spec.offset+spec.length-rdlen+1, spec.offset + spec.length )
			print( format_bounds(closed_bounds), sep="\t", file=fp[0] )
			print( "@{}\n{}\n+\n{}".format( read_id_template.format(i),
				_revcomp( fragment )[:rdlen],
				random_base_qual( rdlen, args )), file=fp[2] )
		else: # single-ended...
			# Just one or the other _end_.
			closed_bounds = ( spec.offset                  +1, spec.offset + rdlen )
			print( format_bounds(closed_bounds), sep="\t", file=fp[0] )
			print( "@{}\n{}\n+\n{}".format( read_id_template.format(i),
				fragment[:rdlen] if random() < 0.5 else _revcomp( fragment[:rdlen] ),
				random_base_qual( rdlen, args )), file=fp[1] )
			

def _generate_bam( genome_path, prefix, fastq:list, args, bwa_stderr ):
	"""
	This merely encapsulate running of bwa followed by samtools.
	"""
	assert isinstance(fastq,list),"...so it's concatenatable to argument list"

	if not _is_indexed( genome_path ):
		cmd = [ _BWA,"index", genome_path ]
		if __debug__:
			print( *cmd, file=stderr )
		run_child( cmd, stderr=bwa_stderr, check=True)

	samfile = "{}.sam".format( prefix )
	bamfile = "{}.bam".format( prefix )

	cmd = [ _BWA, "mem",
		"-R", "@RG\\tID:{}\\tSM:{}".format( args.platform_unit, args.subject ),
		"-a",
		"-o", samfile, genome_path ] + fastq
	if __debug__:
		print( *cmd, file=stderr )
	run_child( cmd, stderr=bwa_stderr, check=True )

	# ...and, finally, sort the alignments and convert to BAM.
	cmd = [ "samtools", "sort", "-o", bamfile, samfile ]
	if __debug__:
		print( *cmd, file=stderr )
	run_child( cmd, stderr=bwa_stderr, check=True )

	cmd = [ "samtools", "index", bamfile ]
	if __debug__:
		print( *cmd, file=stderr )
	run_child( cmd, stderr=bwa_stderr, check=True )


def _main( args ):

	# Create the working directory if required, and open any output file(s) that
	# must be held open.

	if not isdir( args.wdir ):
		mkdir( args.wdir )

	# ...and an entirely homozygous-reference diploid instance thereof.
	haploid_ref_filepath  = path_join( args.wdir, "{}.fa".format( args.ref ) )

	# Create a new reference if and only if the named file does not exist;
	# otherwise load the existing reference.

	if isfile(haploid_ref_filepath):
		named_seqs = _load_fasta( haploid_ref_filepath )
		haploid_ref = [ pair[1] for pair in named_seqs ]

		# contig-lengths and contig-names are optional if a reference is being
		# reused, but if they were supplied check their validity.

		if args.contig_lengths:
			assert all([ len(pair[1]) == args.contig_lengths[i] for i,pair in enumerate(named_seqs) ])
		else:
			args.contig_lengths = [ len(pair[1]) for pair in named_seqs ]

		if args.contig_names:
			assert all([ pair[0] == args.contig_names[i] for i,pair in enumerate(named_seqs) ])
		else:
			args.contig_names = [ pair[0] for pair in named_seqs ]

	else: # Create a random haploid genome...

		haploid_ref = [ [ _random_base() for i in range(clen) ] for clen in args.contig_lengths ]

		if args.mask_ends > 0.0:
			for seq in haploid_ref:
				L = len(seq)
				l = round( uniform(0,args.mask_ends)*L )
				for i in range(   0, l ):
					seq[i] = 'N'
				u = round( uniform(0,args.mask_ends)*L )
				for i in range( L-u, L ):
					seq[i] = 'N'

		# Generate a random mask for internal (motivated by Dragen testing)

		if args.mask_internal > 0.0:
			with open(path_join( args.wdir, "mask.bed" ),"wt") as fp:
				for i,seq in enumerate(haploid_ref):
					L = len(seq)
					l = round( uniform(0,args.mask_internal)*L )
					if l > 0:
						start = randrange(L-l)
						print( args.contig_names[i], start, start+l, sep="\t", file=fp )

		# Optionally append any sequencies explicitly mandated.
		if args.append:
			for name,seq in _load_fasta( args.append ):
				assert len(name) > 0 and len(seq) > 0
				args.contig_names.append( name )
				haploid_ref.append( seq )
				args.contig_lengths.append(len(seq))

		# Emit the "reference" genome (for later use by bwa).

		with open( haploid_ref_filepath, "w" ) as fp:
			for i,chromosome in enumerate(haploid_ref):
				_print_fasta( chromosome, name="{}".format(args.contig_names[i]), dest=fp )

	setattr(args,"genome_length", sum([len(seq) for seq in haploid_ref ]) )
	setattr(args,"contig_count",  len(haploid_ref) )

	subject_path = path_join( args.wdir, "{}.fa".format( args.subject ) )

	# Either load an existing or generate and save a diploid subject's genome.

	if isfile( subject_path ):

		# Load the FASTA file then...

		seq_pairs = _load_fasta( subject_path )

		# ...rearrange into a sequence of homologs assuming the order defined
		# below has not been altered.

		diploid_genome = [ (seq_pairs[2*i+0][1],seq_pairs[2*i+1][1])
			for i in range(len(seq_pairs)//2) ]

		if __debug__:
			names = [ (seq_pairs[2*i+0][0],seq_pairs[2*i+1][0])
				for i in range(len(seq_pairs)//2) ]
			assert all( p[0].split(':')[0] == p[1].split(':')[0] for p in names ),\
				"base chromosomes should be identical between pairs of homologs"

	else: # Generate a new subject

		# Create two "perfect" instantiations of the haploid reference representing
		# a diploid homozygous-reference individual...

		diploid_genome = [ ( chromo.copy(), chromo.copy() ) for chromo in haploid_ref ]

		# ...then generate random variations in it.
		variants = _generate_variation( args, diploid_genome )

		with open( subject_path, "w" ) as fp:
			for i,chromosome in enumerate(diploid_genome):
				for hap in range(2):
					_print_fasta( chromosome[hap], name="{}:homolog{:d}".format(args.contig_names[i],hap), dest=fp )

	prefix = args.prefix.format( args.subject, args.platform_unit )

	with open( path_join( args.wdir, "{}_frags.tab".format(prefix) ), "w" ) as frags_fp:

		if args.single_end:
			fastq = [ path_join( args.wdir, "{}.fq".format( prefix ) ),                     ]
			with open( fastq[0], "w" ) as fastq_fp:
				_simulate_sequencing( args, diploid_genome, ( frags_fp, fastq_fp ) )
		else:
			fastq = [ path_join( args.wdir, "{}.{}.fq".format( prefix, i ) ) for i in (1,2) ]
			with open( fastq[0], "w" ) as fastq_fp1, open( fastq[1], "w" ) as fastq_fp2:
				_simulate_sequencing( args, diploid_genome, ( frags_fp, fastq_fp1, fastq_fp2 ) )

	# Align the generated reads if requested

	if args.bwa_output:
		with open( args.tool_stderr, "w" ) as fp:
			_generate_bam( haploid_ref_filepath, path_join( args.bwa_output, prefix ), fastq, args, fp )

	if len(args.summary) > 0:
		with open( path_join( args.wdir, args.summary ), "w" ) as fp:
			_emit_summary( args, fp )
	else:
		_emit_summary( args, stderr )

############################################################################
# Define and parse command-line arguments

parser = argparse.ArgumentParser( description="Read simulator",
epilog="""Notes:
1. FILENAME arguments may also be "/dev/null" or "/dev/stderr".
2. If the --ref argument (combined with --wdir) refers to an existing
	FASTA file, it is loaded and reused to generate a new subject.
3. Contigs/chromosomes are named by 0-based integers by default.""" )

# Implementation notes:
# 1. The action objects *are* instantiated but not __call__'ed unless the
#		corresponding arguments are provided--that is, they are NOT CALLED for
#		default arguments.
# 2. Thus, default arguments should be given in the form they will be used
#		by subsequent code--that is, NOT as comma-separated strings that still
#		need to be parsed.
# 3. Moreover, they should NOT be typed (using the type argument to
#		add_argument) as that merely triggers unnecessary and wrong type
#		checking and coercion by the parser.
#	4. A small complication in all this, possibly resolvable by more digging
#		into argparse' capabilities, is that the %(default) in help arguments
#		will be whatever Python thinks is an appropriate display.

HELP_SEQ = "Name for {} genome. This will be the base filename of multiple output files. [\"%(default)s\"]"

parser.add_argument( "-r", "--ref", type=str, default="genome",
	help=HELP_SEQ.format("REFERENCE") )
parser.add_argument( "-s", "--subject", type=str, default="subject",
	help=HELP_SEQ.format("SIMULATED") )
parser.add_argument( "-l", "--lengths", action=SplitCommaSepInts, dest='contig_lengths',
	help="Comma-separated sequence of chromosome length(s).[%(default)s]" )
parser.add_argument( "-n", "--names", dest='contig_names',
	help="Comma-separated sequence of chromosome names matching lengths.[%(default)s]" )
parser.add_argument( "-C", "--contigs", type=int,
	help="Number of contigs to generate IFF --lengths and --names unspecified. Otherwise it's random." )
parser.add_argument( "--mask-ends", type=float, default=0.0,
	help="Maximum fraction of contig ends to fill with N's.[%(default)f]" )
parser.add_argument( "--mask-internal", type=float, default=0,
	help="Create a mask BED3 file specifying random internal ranges to mask (for Dragen sim).[%(default)f]" )
parser.add_argument( "-v", "--snvs", type=float, default=1.0,
	help="Either an absolute count of SNVs (if >= 1) or a frequency (per basepair) if (< 1.0). [%(default)f]" )
parser.add_argument( "-i", "--indels", type=float, default=0.0,
	help="Either an absolute count of indels (if >= 1) or a frequency (per basepair) if (< 1.0). [%(default)f]" )
parser.add_argument( "--discrete-quals", type=str,
	help="Assign whole reads constant qualities (e.g. \"DDDDD..\") chosen randomly from this string." )
parser.add_argument( "--snp-base-quals", type=str,
	help="Assign SNP bases qualities chosen randomly from this string." )
#parser.add_argument( "--allele-bias", type=str,
#	help="Assign SNP bases qualities chosen randomly from this string." )

HELP_INTERVAL = "Interval specified as contig:start-end (/([0-9]+):([0-9]+)-([0-9]+)/) {}.[\"%(default)s\"]"

parser.add_argument( "-m", "--mzone", default=None,
	help=HELP_INTERVAL.format( "containing all mutations" ) )
parser.add_argument( "--var-bias", default=0.5, type=float, metavar="PROB",
	help="Bias variation generation to homolog 0 [%(default)f]" )
parser.add_argument( "--frag-bias", default=0.5, type=float, metavar="PROB",
	help="Bias fragment generation to homolog 0 [%(default)f]" )
parser.add_argument( "-D", "--rdnum", type=int, default=1,
	help="Absolute read count. [%(default)d]" )
parser.add_argument( "-d", "--depth", type=int, default=None,
	help="(Simulated) coverage depth. Takes priority over rdnum, if supplied." )

HELP_GAUSS="Integer (mu,sigma) of normally-distributed simulated {} lengths. [N%(default)s]"

parser.add_argument( "-T", "--tlen",  default=[350,30], action=SplitCommaSepInts,
	help=HELP_GAUSS.format("template") )
parser.add_argument( "-R", "--rdlen", default=[150,20], action=SplitCommaSepInts,
	help=HELP_GAUSS.format("read") )
parser.add_argument( "-I", "--intersect", default=None,
	help=HELP_INTERVAL.format( "which every fragment must cover" ) )

parser.add_argument( "--platform-unit", default="", metavar="IDENTIFIER",
	help="Identifier (an infix) for FASTQ filename(s), usually <flowcell>_<lane>. [%(default)s]" )
parser.add_argument( "--prefix", default="{}_{}", metavar="PREFIX",
	help="Prefix template for fragment and FASTQ file name(s) (subject,platform-unit) [%(default)s]" )

parser.add_argument( "--append", metavar="FILENAME",
	help="Append sequence(s) in %(metavar)s to generated contigs." )

parser.add_argument( "-O", "--wdir", type=str, default='output',
	help="Directory for all generated stuff (except BAM files) [\"%(default)s\"]" )

HELP_EXTRA_FILES="""Write the {} to %(metavar)s, if provided, otherwise
to stderr. [\"%(default)s\"]."""

parser.add_argument( "--summary", default="params.txt", metavar="FILENAME",
	help=HELP_EXTRA_FILES.format("arguments summary") )

parser.add_argument( "--bwa", dest="bwa_output", metavar="DIRNAME",
	help="Run bwa on the sample(s) storing output in %(metavar)s" )

parser.add_argument( "--tool-stderr", default="bwa_and_samtools_stderr.txt",
	metavar="FILENAME",
	help=HELP_EXTRA_FILES.format("bwa and samtools stderr") )

parser.add_argument( "--single-end", action="store_true",
	help="Generate single-end reads. Default is paired." )

parser.add_argument( "--seed", type=int,
	help="Seed for random number generation." )

_args = parser.parse_args()

# Transform some arguments and calculate derived attributes.

if _args.seed:
	seed( _args.seed )
	print( "Using", _args.seed, "seed" )

if _args.contig_names is None and _args.contig_lengths is None:
	# Create a _fully_ random genome with 23 + U[0,199] contigs of U[500,10499] bases.
	_args.contig_names = [ "contig{}".format(i+1) for i in range( 23+randrange(100) if _args.contigs is None else _args.contigs ) ]
	_args.contig_lengths = [ 1000+randrange(100000) for _ in range(len(_args.contig_names)) ]
elif _args.contig_names: # If names were specified lengths must also have been. 
	_args.contig_names = _args.contig_names.split(',')
	assert len(_args.contig_names) == len(_args.contig_lengths)
elif _args.contig_lengths: # only lengths were spec'ed, names are indices.
	_args.contig_names = [ str(i) for i in range(len(_args.contig_lengths)) ]
	
# Parse mutation zone into the numeric tuple: (contig,start,end)

if _args.mzone:
	m = re_match( r'(?P<contig>\d+):(?P<start>\d+)-(?P<end>\d+)', _args.mzone )
	if m is None or len(m.groups()) != 3:
		raise ValueError("--mzone argument must match /([0-9]+):([0-9]+)-([0-9]+)/")
	_args.mzone = (
		int(m.group('contig')),
		int(m.group('start')),
		int(m.group('end')) )

_main( _args )

