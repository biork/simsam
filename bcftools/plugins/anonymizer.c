/*
    Copyright (C) 2022 University of Eastern Finland, Heinäniemi Lab

    Author: Roger Kramer <roger.kramer@uef.fi>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <assert.h>
#include <getopt.h>
#include <stdarg.h>
#include <ctype.h>
#include <time.h>

#include "bcftools.h"

static struct random_data rng_data;
static char rng_state[ 8 ];
static int32_t threshold = 0;
static int snvs_only = 0;

const char *about(void) {
    return "Randomize VCF content.\n";
}


const char *usage() {
	return
"About: Anonymizer\n"
"Usage: bcftools +anonymize [General Options] -- [Plugin Options]\n"
"Options:\n"
"   run \"bcftools plugin\" for a list of common options\n"
"\n"
"Plugin options:\n"
"   -s, --seed <int>    to initialize random number generator\n"
"   -d, --drop <float>  skip approx. <float>% of variants at random\n"
"   -1, --snvs          output SNVs only, skip ALL other variants\n"
"\n"
"Example:\n"
"\n"
"  bcftools +anonymizer in.vcf -- -s 1234321\n"
"\n";
}


int init(int argc, char **argv, bcf_hdr_t *in, bcf_hdr_t *out) {

	const time_t NOW = time(NULL);
	unsigned int seed = (unsigned int)NOW; // unless overridden below
	int debug = 0;

	do {
		static const char *CHAR_OPTIONS = "s:d:1D";
		static struct option LONG_OPTIONS[] = {
			{ "seed",  1,0,'s'},
			{ "drop",  1,0,'d'},
			{ "snvs",  0,0,'1'},
			{ "debug", 0,0,'D'},
			{   NULL,  0,0, 0 }
		};
		int opt_index;

		const int c = getopt_long( argc, argv, CHAR_OPTIONS, LONG_OPTIONS, &opt_index );

		if( -1 == c ) break;

		switch( c ) {

		case 's':
			seed = atoi( optarg );
			break;

		case 'D':
			debug = 1;
			break;

		case 'd':
			if( fabs(atof(optarg)) <= 100.0 ) {
				const double limit = (fabs(atof(optarg))/100.0) * (double)RAND_MAX;
				threshold = (int32_t)limit;
			} else {
				error( "drop argument (%s) must be in [0,100]\n", optarg );
			}
			break;

		case '1':
			snvs_only = 1;
			break;

		default:
			error("%s does not understand option\n", __FILE__ );
			return -1;
		}
	} while(1);

	rng_data.state = NULL;
	if( initstate_r( seed, rng_state, sizeof(rng_state), &rng_data ) ) {
		return -1;
	}

	if( debug ) {
		fprintf( stderr, "%d/%d chance of dropping a variant\n", threshold, RAND_MAX );
		error("A hidden testing option was used (accidentally?)\n" );
		return -1;
	}
	return 0;
}


static char random_nuc( char replaced ) {

	/**
		* For reference:
		* A 0x41  a 0x61  0001
		* C 0x43  c 0x63  0011
		* G 0x47  g 0x67  0111
		* T 0x54  t 0x74  0100
		* N 0x4e  n 0x6e  1110
		* Notice bits 1-2 are unique for ACGT (excluding N).
		*/

	static const char *ALTERNATIVES[4] = {
		"CGT", // A => ?
		"AGT", // C => ?
		"ACG", // T => ?
		"ACT"  // G => ?
	};
#define N_ALTS (3)

	int32_t randint;
	if( replaced & 0x08 ) {
		return 'N'; // There already was no info; leave it that way.
	}

	random_r( &rng_data, &randint );
	assert( randint >= 0 );
	return ALTERNATIVES[ (replaced >> 1) & 0x3 ][ randint % N_ALTS ];
}


bcf1_t *process(bcf1_t *rec) {

	const int IS_SNP = bcf_is_snp( rec );

	if( threshold > 0 ) {
		int32_t randint;
		random_r( &rng_data, &randint );
		if( randint < threshold ) {
			return NULL; // meaning don't output anything for this variant.
		}
	}

	if( snvs_only && ! IS_SNP ) {
		return NULL; // meaning don't output anything for this variant.
	}

	if( bcf_unpack( rec, BCF_UN_STR ) == 0 ) {

		if( IS_SNP ) { // obviates another loop so worth checking.

			for(int i = 1; i < rec->n_allele; i++ ) {
				*rec->d.allele[i] = random_nuc( *rec->d.allele[i] );
			}

		} else { // change all bases EXCEPT 1st in each ALT allele.

			for(int i = 1; i < rec->n_allele; i++ ) {

				char *allele = rec->d.allele[i];
				int L = strlen( allele );

				assert( *rec->d.allele[0] == *rec->d.allele[i] );
				// 1st base of ALT allele should always be identical to reference.

				for(int j = 1; j < L; j++ ) {
					rec->d.allele[i][j] = random_nuc( rec->d.allele[i][j] );
				}
			}
		}
	}

	// Tell caller (bcftools) the record HAS changed and needs repacking.
	rec->d.shared_dirty = 1;
	return rec;
}


void destroy(void) {
}

