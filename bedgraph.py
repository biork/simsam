
from random import randrange as rr
from CDS import HalfOpenBounds

def random( upto, **kwargs ):
	"""
	Generate random bedgraph data
	chro
	"""
	gap_sizes = kwargs.get( 'gap', HalfOpenBounds(0,50) )
	span_sizes = kwargs.get( 'span', HalfOpenBounds(10,50) )
	value = kwargs.get( 'value', HalfOpenBounds(1,100) )
	cb = kwargs.get( 'callback', None )

	MIN_PAIR = gap_sizes.min + span_sizes.min

	if not cb:
		accum = []

	pos = 0
	while pos + MIN_PAIR < upto:
		start = min( pos + rr(*gap_sizes.bounds), upto )
		if upto - start < span_sizes.min:
			break
		end = min( start + rr(*span_sizes.bounds), upto )
		span = ( start, end, rr( *value.bounds ) )
		if cb:
			cb( span )
		else:
			accum.append( span )
		pos = end
	if cb is None:
		return accum


if __name__=="__main__":
	from sys import argv
	random( int(argv[1]) if len(argv) > 1 else 10000, callback=lambda t:print(*t,sep="\t") )
