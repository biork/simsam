
# RAndom BIological Data

Generate random biological data of various types, primarily for software
testing. Data is intended to be representative of real data, but not
necessarily _probable_--that is, this is _not_ intended to test statistical
methods or models of biological processes. It is only intended to generate
reasonably representative data for analysis software's ingestion.

It is _not_ intended to test:
1. statistical methods
2. mutational models
3. scalability. (It generates small-ish simulated data.)

The overriding goal is simplicity of use for sanity checking software
with genuinely random data.

