
from random import uniform,choice,random,randrange,gauss
from os.path import join as path_join

def _random_base():
	"""
	Draw a random base given the (not necessarily uniform) probability masses
	in the _BASES_P.
	"""
	global _BASE_SUM_P,_BASES_CUM_P
	rn = uniform(0,_BASE_SUM_P)
	for i in range(len(_BASES_CUM_P)-1,0,-1):
		if rn > _BASES_CUM_P[i]:
			return "ACGT"[i]
	return "A"


class RandomSubintervalParams:
	"""
	Contains parameters constraining the generation of subintervals.
	1. size
		1. uniform in [min,max]
		2. truncated Gaussian in [mu,sd]
			mu,sd > 1 are used absolutely
			mu,sd ∈ (0,1) are treated as a fraction of parent length
	2. min_flank -- positive value insures the interval is proper subinterval
	The constraints are applied with the following precedence.
	"""
	def __init__( self, parent_len, args ):
		"""
		Confine to fixed range of parent interval.
		Randomize size _relative to parent_ from Gaussian (mu,sd).
		Insure result is subinterval
		"""
		min_size = float( getattr(args,"min_size",1) )
		assert min_size > 0
		if min_size >= 1:
			self.min_size = min( int(min_size), parent_len )
		else:
			self.min_size = 

		max_size = float( getattr(args,"max_size",parent_len) )
		self.start

	@property
	def max_len( self ):
		return self.max_size

	def __call__( self ) -> (int,int):
		"""
		Generate a random subinterval subject to self's parameters.
		"""
		pass


def _random_subinterval( start=0, minlen=1 ):

class Reference:
	@staticmethod
	def random( args:"named arguments from argparse" ):
		"""
		Generate n random contigs of specified lengths

		"""
		self.contig = [ [ _random_base() for i in range(l) ] for l in args.contig_lengths ]
		# Generate one alt region per contig consisting of 1/10
		self.filepath = path_join( args.wdir, "{}.fa".format( args.ref ) )

	def __init__( self ):
		pass

	def dd():
		a


