
# Generate a:
# 1. random reference,
# 2. subject with SNPs, and...
# 3. three sequencing runs from that (reference,subject) pair.

SIMSAM="${HOME}/projects/simsam/simsam.py"
SUBJECT="D_17"
REFNAME="dummy"

python3 ${SIMSAM} \
	-l $((2**17)),$((2**16)) \
	-n chrA,chrB \
	-v 0.001 \
	-d 30 \
	--ref ${REFNAME} \
	--subject ${SUBJECT} \
	--platform-unit HDABXXYFK_3

# The reference (dummy.fa) and subject (D_17.fa) from above are reused
# for two more sequencing "runs" below because the files already exist.

sleep 1

python3 ${SIMSAM} \
	-d 30 \
	--ref ${REFNAME} \
	--subject ${SUBJECT} \
	--platform-unit JXAFGGHEY_1

sleep 1

python3 ${SIMSAM} \
	-d 30 \
	--ref ${REFNAME} \
	--subject ${SUBJECT} \
	--platform-unit JXAFGGHEY_2

# The sleeps above insure that following line can confirm that
# dummy.fa and D_17.fa were only written once.

stat -c "%Z %n" output/* | sort

pushd output
gzip *.fq
popd
