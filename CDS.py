
"""
This library generates random CDS (coding sequence) annotation.

Specifically this means it...
1. generates genes which approximately fill a given chromosome length.
2. Genes on opposite strands may overlap arbitrarily.
3. Genes on the same strand never overlap.
4. Each gene has a random intron-exon structure as define in 
"""
from random import random,randrange as rr,randint

# Following minimums put lower bounds on exon sizes.
# These should be biologically plausible, not probable, values.
_MIN_CDS_LEN  = 20
_MIN_5UTR_LEN = 1
_MIN_3UTR_LEN = 1
_MIN_UTR_LEN  = _MIN_5UTR_LEN + _MIN_3UTR_LEN

ABS_MIN_EXON_LEN = _MIN_CDS_LEN + _MIN_UTR_LEN

def random_alphanum( **args ):
	"""
	[A-Z]{3,}[0-9]{0,2}
	"""
	l = max(3,args.get('len',5))
	suffix = str(rr(1,100)) if random() < 0.5 else ""
	return ''.join([chr(ord('A')+rr(0,26)) for _ in range(l-len(suffix))]) + suffix


def genomic_offsets_of( transcript_offset, lengths ) -> [int,]:
	"""
	Given a gene's structure [exon,intron,...,exon] as a series of lengths,
	find the GENOMIC offsets of the given TRANSCRIPT offsets.
	"""
	assert all( l > 0 for l in lengths )
	results = [ None for _ in transcript_offset ]
	ti = 0
	N = len(transcript_offset)
	genome_offset = 0
	consumed = 0
	for i,l in enumerate(lengths):
		if i % 2 == 0: # we're in an exon.
			while ti < N and transcript_offset[ti] < consumed + l:
				results[ti] = genome_offset + (transcript_offset[ti]-consumed)
				ti += 1
			consumed += l
			if ti >= N: # we're actually done!
				break
		genome_offset += l # ALWAYS increment the genomic offset
	return results # ...whatever we finished; some may still be None!


class HalfOpenBounds(object):
	"""
	This can represent multiple concepts, in particular:
	1. a fixed range of a 1D coordinate integer space (e.g. genome), or
	2. a sample-space of values (e.g. admissable sizes)
	Properties are reflect the different uses.
	"""
	def __init__( self, a, b=None ):
		if b is not None:
			# Both must be convertible to ints
			self._bounds = tuple([int(a),int(b)])
		elif isinstance(a,list):
			self._bounds = tuple(a)
		else:
			assert isinstance(a,str)
			self._bounds = tuple([ int(v) for v in a.split(',') ])
		assert self.min < self.upperbound

	def __len__( self ):
		return self._bounds[1] - self._bounds[0]

	def __str__( self ):
		return "[{},{})".format(*self._bounds)

	def __contains__( self, val ) -> bool:
		if isinstance(val,int):
			return self.min <= val and val < self.upperbound
		elif hasattr(val,"__iter__"):
			return all( self.min <= v and v < self.upperbound for v in val ) 
		else:
			raise TypeError("expected Number or iterable of Numbers")

	def __iter__( self ):
		return iter(self._bounds)

	@property
	def min( self ):
		return self._bounds[0]

	@property
	def start( self ):
		return self._bounds[0]

	@property
	def upperbound( self ):
		return self._bounds[1]

	@property
	def end( self ):
		return self._bounds[1]

	@property
	def bounds( self ):
		return self._bounds


class GenomicInterval(HalfOpenBounds):
	"""
	A HalfOpenBounds that includes strand.
	"""
	def __init__( self, s, e, forward=True ):
		super().__init__(s,e)
		if isinstance(forward,bool):
			self.forward = forward
		else:
			assert isinstance(forward, str)
			self.forward = (forward == "+")

	@property
	def strand( self ) -> str:
		return "+" if self.forward else "-"

	def __str__( self ):
		return super().__str__() + ":" + self.strand


class Gene(GenomicInterval):
	"""
	A GenomicInterval that includes a name, internal intron/exon partition,
	and CDS definition.
	A gene is a partition of a region of contig into [e,i,e,i,...,e]
	beginning AND ENDING with _exons_.
	"""
	@staticmethod
	def random( start, upper_bound, **kwargs ):
		"""
		kwargs can include:
		1. a HalfOpenBounds called intron bounding intron sizes
		2. a HalfOpenBounds called exon bounding exon sizes
		3. a name
		4. a strand specification
		...but all are optional.
		Create a random 
		- partition [s,e) into random series of alternating intr/exonic regions
		- random exon sizes in args.exon_sizes
		- random intron sizes in args.intron_sizes
		- random CDS bounds within first and last exons
		"""
		assert start < upper_bound
		intron_size = kwargs.get('intron',HalfOpenBounds(1000,10000) )
		exon_size   = kwargs.get('exon',  HalfOpenBounds(  50,500  ) )
		PAIR_MIN = intron_size.min + exon_size.min
		MAX_GENE_LENGTH = upper_bound - start
		# Generate the partition, staying strictly within [start, upper_bound)
		# and insuring exons are on both ends by trying to exhaust the interval
		# and exiting loop early when constraints are unsatisfiable by an
		# additional intron and/or exon.
		lengths = [] # of intervals in the partition
		remainder = MAX_GENE_LENGTH
		while remainder > 0:
			exonic = len(lengths) % 2 == 0
			# Append another interval insuring we stay within bounds.
			size = min( remainder,
				rr( *(exon_size if exonic else intron_size).bounds ) )
			lengths.append( size )
			remainder = MAX_GENE_LENGTH - sum(lengths)
			# Do we need to exit early, possibly amending the tail?
			if exonic:
				#  Does room remain for another intron+exon _pair_?
				if remainder < PAIR_MIN:
					break # ...though, gene may be shorter than MAX_GENE_LENGTH.
			else: # we just added an intron.
				# Does room remain for another exon?
				if remainder < exon_size.min:
					# Insufficient remainder for another exon, truncate at the
					# previous exon by discarding the last intron and quit.
					lengths.pop(-1)
					break
		# [e(,i,e)*] structure implies interval count is always odd.
		assert len(lengths) % 2 == 1
		FINAL_LENGTH = sum(lengths)
		assert FINAL_LENGTH <= MAX_GENE_LENGTH
		########################################################################
		# Generate a random CDS by taking a proper subinterval of the transcript
		# length. This insures case of single-exon is satisfied!
		transcript_length = sum( lengths[i] for i in range(0,len(lengths),2) )
		assert transcript_length >= ABS_MIN_EXON_LEN
		cds_len = rr( _MIN_CDS_LEN, transcript_length - _MIN_UTR_LEN )
		cds_off = _MIN_5UTR_LEN + rr( 0, transcript_length - cds_len - _MIN_3UTR_LEN ) 
		# Now find where CDS ends must be in _genomic_ coordinates.
		genomic_off = genomic_offsets_of([ cds_off, cds_off+cds_len], lengths )
		assert all( not o is None for o in genomic_off )
		########################################################################
		return Gene(
			start,
			start+FINAL_LENGTH,
			kwargs.get('strand',random() < 0.5),
			# Use the provided name, if any, otherwise 5 random capital letters.
			kwargs.get('name',''.join( chr(ord('A')+rr(0,26)) for _ in range(5) ) ),
			lengths,
			HalfOpenBounds( start+genomic_off[0], start+genomic_off[1] ) )

	def __init__( self, start, end, isfwd, name, partition, cds ):
		super().__init__( start, end, isfwd )
		self.name = name
		assert all( isinstance(v,int) and v > 0 for v in partition )
		self.partition = partition
		self._exons = []
		# Create an array of this gene's exons.
		pos = start
		for i,l in enumerate(partition):
			if i % 2 == 0: # if i is index of an exon
				self._exons.append( HalfOpenBounds(pos,pos+l) )
			pos += l
		assert all( any( pos in e for e in self._exons ) for pos in cds )
		self.cds = cds

	@property
	def exons( self ):
		return self._exons

	def __str__( self ):
		return '\t'.join([
			str(self.start),
			str(self.end),
			self.strand,
			self.name,
			str(self.cds),
			','.join([str(e.start) for e in self._exons ]),
			','.join([str(e.end  ) for e in self._exons ])])


def random_annotation( upper_bound, **kwargs ):
	"""
	Generate a random collection of genes on a chromosome of length upper_bound.

	kwargs can include those used by Gene.random as well as:
	1. 'gene', a HalfOpenBounds bounding gene sizes
	2. 'callback' giving a function that will be called with each generated
		gene instead of accumulating them into a list.
	Genes may overlap on different strands, not on same strand.
	"""
	gene_sizes       = kwargs.get( 'gene',       HalfOpenBounds(100,20000) )
	intergenic_sizes = kwargs.get( 'intergenic', HalfOpenBounds(100,20000) )
	cb = kwargs.get( 'callback', None )

	lower_bound = [0,0]

	fwd = random() < 0.5
	start = rr( *intergenic_sizes.bounds )
	end = min( start + rr( *gene_sizes.bounds ), upper_bound )
	last = Gene.random( start, end, strand=fwd, name=random_alphanum(), **kwargs )
	lower_bound[ 0 if fwd else 1 ] = last.end # ...which may be < end
	if cb:
		cb( last )
	else:
		accum = [last,]

	while True:
		fwd = random() < 0.5
		# Cannot overlap the last gene on _same_ strand, so...
		start = lower_bound[ 0 if fwd else 1 ] + rr( *intergenic_sizes.bounds )
		end = min( start + rr( *gene_sizes.bounds ), upper_bound )
		if end-start < gene_sizes.min:
			break
		g = Gene.random( start, end, strand=fwd, name=random_alphanum(), **kwargs )
		lower_bound[ 0 if fwd else 1 ] = g.end # ...which may be < end
		if cb:
			cb(g)
		else:
			accum.append( g )
		last = g
	if cb is None:
		return accum


if __name__=="__main__":
	random_annotation( 100000, callback=lambda g:print(g) )

